# Copyright (C) 2005-2018 Thomas Nagy (ita)
# Copyright (C) 2023-2024 Björn A. Lindqvist (bjourne)
"""
Load this tool to use pkg-config-like programs.
"""
from waflib import Logs
from waflib.Configure import conf
from waflib.Tools.c_config import parse_c_flags, set_uselib_c_flags
from waflib.Utils import quote_define_name, to_list

@conf
def exec_cfg(
        self,
        path,
        pkg_config_path = None,
        atleast_pkgconfig_version = None,
        modversion = None,
        # Don't think this is used
        define_variable = None,
        force_static = False,
        package = None,
        args = [],
        variables = [],
        define_name = None,
        uselib_store = None,
        posix = True,
        global_define = True,
        okmsg = None):

    """Executes ``pkg-config`` or other ``-config`` applications to
    collect configuration flags:

    * if atleast_pkgconfig_version is given, check that pkg-config has
      the version n and return
    * if modversion is given, then return the module version
    * else, execute the *-config* program with the *args* and
      *variables* given, and set the flags on the
      *conf.env.FLAGS_name* variable

    :param path: the **-config program to use**
    :type path: list of string
    :param atleast_pkgconfig_version: minimum pkg-config version to
        use (disable other tests)
    :type atleast_pkgconfig_version: string
    :param package: package name, for example *gtk+-2.0*
    :type package: string
    :param uselib_store: if the test is successful, define
        HAVE\\_*name*. It is also used to define *conf.env.FLAGS_name*
        variables.
    :type uselib_store: string
    :param modversion: if provided, return the version of the given
        module and define *name*\\_VERSION
    :type modversion: string
    :param args: arguments to give to *package* when retrieving flags
    :type args: list of string
    :param variables: return the values of particular variables
    :type variables: list of string
    :param define_variable: additional variables to define (also in
        conf.env.PKG_CONFIG_DEFINES)
    :type define_variable: dict(string: string)
    :param pkg_config_path: paths where pkg-config should search for
        .pc config files (overrides env.PKG_CONFIG_PATH if exists)
    :type pkg_config_path: string, list of directories separated by
        colon
    :param force_static: force usage of static libraries
    :type force_static: bool default False
    :param posix: usage of POSIX mode for shlex lexical analysis library
    :type posix: bool default True

    """
    assert type(path) == list
    run_env = self.env.env or None
    if pkg_config_path:
        if not run_env:
            run_env = dict(self.environ)
            run_env["PKG_CONFIG_PATH"] = pkg_config_path

    def define_it():
        # by default, add HAVE_X to the config.h, else provide
        # DEFINES_X for use=X
        if global_define:
            self.define(define_name, 1, False)
        else:
            self.env.append_unique(
                "DEFINES_%s" % uselib_store,
                "%s=1" % define_name
            )

        self.env[define_name] = 1

    # pkg-config version
    if atleast_pkgconfig_version is not None:
        cmd = path + [
            "--atleast-pkgconfig-version=%s" % atleast_pkgconfig_version
        ]
        self.cmd_and_log(cmd, env=run_env)
        return

    # single version for a module
    if modversion is not None:
        version = self.cmd_and_log(
            path + ["--modversion", modversion], env=run_env
        ).strip()
        if okmsg:
            okmsg = version
        self.define(define_name, version)
        return version

    lst = [] + path

    defi = define_variable
    if not defi:
        defi = self.env.PKG_CONFIG_DEFINES or {}
    for key, val in defi.items():
        lst.append("--define-variable=%s=%s" % (key, val))

    if "--static" in args or "--static-libs" in args:
        force_static = True
    lst += args

    # tools like pkgconf expect the package argument after the -- ones -_-
    if package:
        assert type(package) == str
        lst.append(package)

    # retrieving variables of a module
    if variables:
        v_env = env or self.env
        vars = to_list(variables)
        for v in vars:
            val = self.cmd_and_log(lst + ["--variable=" + v], env=run_env).strip()
            var = "%s_%s" % (uselib_store, v)
            v_env[var] = val
        return

    # so we assume the command-line will output flags to be parsed afterwards
    ret = self.cmd_and_log(lst, env=run_env)
    define_it()

    is_msvc = self.env["CXX_NAME"] == "msvc"
    d = parse_c_flags(ret, posix, force_static, is_msvc)
    set_uselib_c_flags(self.env, uselib_store, d)

    return ret

@conf
def check_cfg(
        self,
        path = None,

        # Messages
        msg = None, okmsg = None, errmsg = None,

        # What we're checking for
        atleast_pkgconfig_version = None,
        modversion = None,
        package = None,
        atleast_version = None,

        # Modifies the results we get
        force_static = False,
        args = [],
        pkg_config_path = None,

        # Where and how we store it
        global_define = True,
        uselib_store = None,
        define_name = None
):
    """
    Checks for configuration flags using a **-config**-like
    program (pkg-config, sdl-config, etc).

    This wraps internal calls to
    :py:func:`waflib.Tools.c_config.validate_cfg` and
    :py:func:`waflib.Tools.c_config.exec_cfg` so check exec_cfg
    parameters descriptions for more details on kw passed

    A few examples::

        def configure(conf):
            conf.load_tool('compiler_c')
            conf.check_cfg(package='glib-2.0', args='--libs --cflags')
            conf.check_cfg(package='pango')
            conf.check_cfg(
                package='pango',
                uselib_store='MYPANGO',
                args=['--cflags', '--libs']
            )
            conf.check_cfg(
                package='pango',
                args=[
                    'pango >= 0.1.0',
                    'pango < 9.9.9',
                    '--cflags', '--libs'],
                msg="Checking for 'pango 0.1.0'")
            conf.check_cfg(
                path='sdl-config',
                args='--cflags --libs',
                package='',
                uselib_store='SDL')
            conf.check_cfg(
                path='mpicc',
                args=['--showme:compile', '--showme:link'],
                package='',
                uselib_store='OPEN_MPI',
                mandatory=False)
            # variables
            conf.check_cfg(
                package='gtk+-2.0',
                variables=['includedir', 'prefix'],
                uselib_store='FOO')
            print(conf.env.FOO_includedir)
    """
    assert type(args) == list
    # Set path
    if not path:
        if not self.env.PKGCONFIG:
            self.find_program("pkg-config", var="PKGCONFIG")
        path = self.env.PKGCONFIG
    path = to_list(path)

    # Set checking message
    if not msg:
        if atleast_pkgconfig_version:
            msg = "Checking for 'pkg-config' version >= %r" \
                % atleast_pkgconfig_version
        elif modversion:
            msg = "Checking for %r version" % modversion
        else:
            msg = "Checking for %r" % package

    # If using pkg-config, verify that exactly one action is
    # requested.
    if not path:
        lst = [atleast_pkgconfig_version, modversion, package]
        cnt = len([e for e in lst if e is not None])
        if cnt != 1:
            raise ValueError(
                "Exactly one of atleast_pkgconfig_version, modversion and package must be set"
            )

    # Set ok and errmsg
    if not okmsg and not modversion:
        okmsg = "yes"
    if not errmsg:
        errmsg = "not found"

    # Set define_name and uselib_store
    if atleast_pkgconfig_version:
        pass
    elif modversion:
        if not uselib_store:
            uselib_store = modversion
        if not define_name:
           define_name = "%s_VERSION" \
                % quote_define_name(uselib_store)
    else:
        if not uselib_store:
            uselib_store = to_list(package)[0].upper()
        if not define_name:
            define_name = self.have_define(uselib_store)

    if msg:
        self.start_msg(msg)
    ret = None
    try:
        ret = self.exec_cfg(
            path,
            atleast_pkgconfig_version = atleast_pkgconfig_version,
            package = package,
            modversion = modversion,
            force_static = force_static,
            args = args,
            pkg_config_path = pkg_config_path,
            uselib_store = uselib_store,
            global_define = global_define,
            define_name = define_name,
            okmsg = okmsg
        )
    except self.errors.WafError as e:
        if errmsg:
            self.end_msg(errmsg, "YELLOW")
        if Logs.verbose > 1:
            self.to_log("Command failure: %s" % e)
        self.fatal("The configuration failed")
    if okmsg:
        self.end_msg(okmsg)
    if not ret:
        ret = True
    return ret
