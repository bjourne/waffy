# Copyright (C) 2005-2018 Thomas Nagy (ita)
# Copyright (C) 2023-2025 Björn A. Lindqvist (bjourne)
"""
C/C++/D configuration helpers
"""
import os, re, shlex
from waflib import Build, Utils, Task, Options, Logs, Errors, Runner
from waflib.TaskGen import after_method, feature
from waflib.Configure import conf

WAF_CONFIG_H = "config.h"
"""default name for the config.h file"""

DEFKEYS = "define_key"
INCKEYS = "include_key"

SNIP_EMPTY_PROGRAM = """
int main(int argc, char **argv) {
    (void)argc; (void)argv;
    return 0;
}
"""

MACRO_TO_DEST_OS = {
    "__linux__": "linux",
    "__GNU__": "gnu",  # hurd
    "__FreeBSD__": "freebsd",
    "__NetBSD__": "netbsd",
    "__OpenBSD__": "openbsd",
    "__sun": "sunos",
    "__hpux": "hpux",
    "__sgi": "irix",
    "_AIX": "aix",
    "__CYGWIN__": "cygwin",
    "__MSYS__": "cygwin",
    "_UWIN": "uwin",
    "_WIN64": "win32",
    "_WIN32": "win32",

    # Note about darwin: this is also tested with
    # 'defined __APPLE__ && defined __MACH__' somewhere below in this
    # file.
    "__ENVIRONMENT_MAC_OS_X_VERSION_MIN_REQUIRED__": "darwin",
    "__ENVIRONMENT_IPHONE_OS_VERSION_MIN_REQUIRED__": "darwin",  # iphone
    "__QNX__": "qnx",
    "__native_client__": "nacl",  # google native client platform
}

MACRO_TO_DEST_CPU = {
    "__x86_64__": "x86_64",
    "__amd64__": "x86_64",
    "__i386__": "x86",
    "__ia64__": "ia",
    "__mips__": "mips",
    "__sparc__": "sparc",
    "__alpha__": "alpha",
    "__aarch64__": "aarch64",
    "__thumb__": "thumb",
    "__arm__": "arm",
    "__hppa__": "hppa",
    "__powerpc__": "powerpc",
    "__ppc__": "powerpc",
    "__convex__": "convex",
    "__m68k__": "m68k",
    "__s390x__": "s390x",
    "__s390__": "s390",
    "__sh__": "sh",
    "__xtensa__": "xtensa",
    "__e2k__": "e2k",
    "__riscv": "riscv",
}

# We don't map these to "readable names" since the macro names
# themselves are descriptive.
DEST_CPU_EXTS_MACROS = {
    '__ADX__',
    '__AVX__',
    '__AVX2__',
    '__AVX512CD__',
    '__AVX512ER__',
    '__AVX512F__',
    '__AVX512PF__',
    '__FMA__',
    '__POPCNT__',
    '__SSE__',
    '__SSE2__',
    '__SSE3__',
    '__SSE4_1__',
    '__SSE4_2__'
}

def ordered_set(lst):
    return list({e:0 for e in lst}.keys())

KNOWN_FLAG_STARTERS = (
    # Machine options
    "-m",
    # Optimization and debug control
    "-f", "-O", "-g",
    # Preprocessor options
    "-Wp,",
    # Warnings
    "-W"
)

def parse_c_flags(line, posix, force_static, is_msvc):
    """Parses C and C++-related command-line flags into a dictionary::

        def configure(conf):
            d = conf.parse_flags('-O3', True, False, False)

    The function raises a ValueError if unknown flags are found.

    :param line: flags
    :type line: string
    :param force_static: force usage of static libraries
    :type force_static: bool default False
    :param posix: usage of POSIX mode for shlex lexical analiysis library
    :type posix: bool default True
    """
    from shlex import shlex

    cxx_flags = []
    c_flags = []
    link_flags = []
    libs = []
    lib_paths = []
    stlibs = []
    stlib_paths = []
    includes = []
    defines = []

    lex = shlex(line, posix = posix)
    lex.whitespace_split = True
    lex.commenters = ""

    toks = list(lex)
    while toks:
        tok = toks.pop(0)
        st, rem = tok[:2], tok[2:]
        if tok.startswith(KNOWN_FLAG_STARTERS):
            c_flags.append(tok)
            cxx_flags.append(tok)
        elif st in {"-I", "/I"}:
            if not rem:
                rem = tok.pop(0)
            includes.append(rem)
        elif st == "-D" or (st == "/D" and is_msvc):
            if not rem:
                rem = tok.pop(0)
            defines.append(rem)
        elif st == "-l":
            if not rem:
                rem = tok.pop(0)
            lst = stlibs if force_static else libs
            lst.append(rem)
        elif st == "-L":
            if not rem:
                rem = tok.pop(0)
            lst = stlib_paths if force_static else lib_paths
            lst.append(rem)
        elif tok.startswith("+") or tok in {
                "-pthread",
                "-fPIC",
                "-fpic",
                "-fPIE",
                "-fpie",
                "-flto",
                "-fno-lto",
                "-pipe"}:
            c_flags.append(tok)
            cxx_flags.append(tok)
            link_flags.append(tok)
        else:
            raise ValueError("Unknown flag %s!" % tok)

    return {
        "CFLAGS" : c_flags,
        "CXXFLAGS" : cxx_flags,
        "LINKFLAGS" : link_flags,
        "DEFINES" : defines,
        "INCLUDES" : ordered_set(includes),
        "LIB" : libs,
        "LIBPATH" : ordered_set(lib_paths),
        "STLIB" : stlibs,
        "STLIBPATH" : stlib_paths,
    }

def set_uselib_c_flags(env, uselib, parsed_flags):
    for k, new in parsed_flags.items():
        key = "%s_%s" % (k, uselib)
        old = env[key]
        if old:
            if new:
                fmt = "Redefition of %s: %s -> %s."
                raise ValueError(fmt % (key, old, new))
        else:
            env[key] = new

def build_fun(bld):
    """Build function that is used for running configuration tests
    with ``conf.check()``
    """
    compile_filename = bld.kw["compile_filename"]

    if compile_filename:
        node = bld.srcnode.make_node(compile_filename)
        node.write(bld.kw["code"])

    o = bld(
        features=bld.kw["features"],
        source=compile_filename,
        target="testprog",
    )

    for k, v in bld.kw.items():
        setattr(o, k, v)

    if not bld.kw.get("quiet"):
        bld.conf.to_log("==>\n%s\n<==" % bld.kw["code"])

def humanize_list(lst):
    n = len(lst)
    if n == 1:
        return lst[0]
    elif n == 2:
        return " and ".join(lst)
    return ", ".join(lst[:-1]) + ", and " + lst[-1]

def get_check_message(kw):
    existing = kw.get("msg")
    if existing is not None:
        return existing

    checkables = [
        ("framework", "framework_name"),
        ("header", "header_name"),
        ("library", "lib"),
        ("static library", "stlib"),
        ("code snippet", "fragment"),
        ("C++ compiler flag(s)", "cxxflags"),
        ("C compiler flag(s)", "cflags"),
        ("D compiler flag(s)", "dflags"),
        ("linker flags", "linkflags")
    ]
    hidden = {"fragment"}
    frags = []
    for s, key in checkables:
        v = kw.get(key)
        if v:
            v = humanize_list(v) if isinstance(v, list) else v
            frag = f"{s} {v}" if not key in hidden else s
            frags.append(frag)
    assert frags
    return "Checking for " + humanize_list(frags)

def get_uselib_store(kw):
    existing = kw.get("uselib_store")
    if existing:
        return existing
    if "framework_name" in kw:
        fwkname = kw["framework_name"]
        return fwkname.upper()
    if "lib" in kw:
        return kw["lib"].upper()
    if "stlib" in kw:
        return kw["stlib"].upper()
    hdr = kw.get("header_name")
    if hdr:
        return hdr.upper()
    return None

# Features is used in post_check to determine which variables to set.
def get_features(kw):
    features = kw.get("features", [])
    if not features:
        type = kw["type"]
        features = kw["compilers"]
        if not "header_name" in kw or kw.get("link_header_test", True):
            features.append(type)
    if kw.get("execute"):
        features.append("test_exec")
    return features

def validate_c(self, kw):
    """Pre-checks the parameters that will be given to
    :py:func:`waflib.Configure.run_build`

    :param compilers: c or cxx (tries to guess what is best)
    :type compilers: list of strings
    :param type: cprogram, cshlib, cstlib - not required if *features
        are given directly*
    :type type: binary to create
    :param feature: desired features for the task generator that will
        execute the test, for example ``cxx cxxstlib``
    :type feature: list of string
    :param fragment: provide a piece of code for the test (default is
        to let the system create one)
    :type fragment: string
    :param uselib_store: define variables after the test is
        executed (IMPORTANT!)
    :type uselib_store: string
    :param use: parameters to use for building (just like the normal
        *use* keyword)
    :type use: list of string
    :param define_name: define to set when the check is over
    :type define_name: string
    :param execute: execute the resulting binary
    :type execute: bool
    :param define_ret: if execute is set to True, use the execution
        output in both the define and the return value
    :type define_ret: bool
    :param header_name: check for a particular header
    :type header_name: string
    :param auto_add_header_name: if header_name was set, add the headers
        in env.INCKEYS so the next tests will include these headers
    :type auto_add_header_name: bool
    """
    # Interim
    assert not "compiler" in kw
    assert not "compile_mode" in kw
    assert type(kw.get("features", [])) == list

    kw["msg"] = get_check_message(kw)
    kw["uselib_store"] = get_uselib_store(kw)

    kw["errmsg"] = kw.get("errmsg", "no")
    kw["okmsg"] = kw.get("okmsg", "yes")
    kw["build_fun"] = kw.get("build_fun", build_fun)
    for x in ("type_name", "field_name", "function_name"):
        if x in kw:
            Logs.warn("Invalid argument %r in test" % x)

    if not "env" in kw:
        kw["env"] = self.env.derive()
    env = kw["env"]

    compilers = kw.get("compilers", [])
    if not compilers and not "features" in kw:
        if env["CXX_NAME"] and Task.classes.get("cxx") and self.env["CXX"]:
            compilers.append("cxx")
        if env["CC_NAME"] and Task.classes.get("c") and self.env["CC"]:
            compilers.append("c")
    kw["compilers"] = compilers

    # C unless otherwise specified (fix this)
    kw["type"] = kw.get("type", "cprogram")
    kw["execute"] = "execute" in kw

    kw["features"] = get_features(kw)

    if not "compile_filename" in kw:
        ext = "cpp" if "cxx" in kw["compilers"] else "c"
        kw["compile_filename"] = "test." + ext

    def to_header(dct):
        if "header_name" in dct:
            dct = Utils.to_list(dct["header_name"])
            return "".join(["#include <%s>\n" % x for x in dct])
        return ""

    header_name = kw.get("header_name")
    if "framework_name" in kw:
        # OSX, not sure this is used anywhere
        fwkname = kw["framework_name"]
        if not kw.get("no_header"):
            fwk = "%s/%s.h" % (fwkname, fwkname)
            if kw.get("remove_dot_h"):
                fwk = fwk[:-2]
            val = kw.get("header_name", [])
            kw["header_name"] = Utils.to_list(val) + [fwk]
        kw["framework"] = fwkname

    elif header_name:
        l = Utils.to_list(kw["header_name"])
        assert len(l), "list of headers in header_name is empty"
        kw["code"] = to_header(kw) + SNIP_EMPTY_PROGRAM

    if "fragment" in kw:
        # an additional code fragment may be provided to replace the
        # predefined code in custom headers
        kw["code"] = kw["fragment"]

    if kw["execute"]:
        kw["chmod"] = Utils.O755

    kw["define_name"] = get_have_define(kw)
    if kw["define_name"]:
        self.undefine(kw["define_name"])

    kw["code"] = kw.get("code", SNIP_EMPTY_PROGRAM)

    # if there are headers to append automatically to the next tests
    if self.env[INCKEYS]:
        kw["code"] = (
            "\n".join(["#include <%s>" % x for x in self.env[INCKEYS]])
            + "\n"
            + kw["code"]
        )

    # in case defines lead to very long command-lines
    if kw.get("merge_config_header") or env.merge_config_header:
        kw["code"] = "%s\n\n%s" % (self.get_config_header(), kw["code"])
        env["DEFINES"] = []  # modify the copy

    kw["success"] = kw.get("success")


def get_uselib_config(uselib_store, kw):
    from waflib.Tools import ccroot
    vars = [ccroot.USELIB_VARS[x] for x in kw["features"]]
    vars = set().union(*vars)
    kw = {k.upper() : v for (k, v) in kw.items()}
    vars = vars.intersection(kw)
    for v in vars:
        key = v + "_" + uselib_store
        val = kw[v]
        yield key, val

def get_have_define(kw):
    define_name = kw.get("define_name")
    if define_name:
        return define_name

    attrs = ["uselib_store", "lib", "header_name"]
    for attr in attrs:
        val = kw.get(attr)
        if val:
            name = Utils.to_list(val)[0]
            return "HAVE_%s" % Utils.quote_define_name(name)
    assert False


@conf
def post_check(self, **kw):
    """
    Sets the variables after a test executed in
    :py:func:`waflib.Tools.c_config.check` was run successfully
    """
    uselib_store = kw.get("uselib_store")
    execute = kw["execute"]
    global_define = kw.get("global_define", 1)
    header_name = kw.get("header_name")
    define_name = kw.get("define_name")
    define_ret = kw.get("define_ret")
    quote = kw.get("quote", 1)
    success = kw.get("success")
    features = kw.get("features")

    is_success = 0
    if execute:
        if success is not None:
            if define_ret:
                is_success = success
            else:
                is_success = success == 0
    else:
        is_success = success == 0

    if define_name:
        comment = kw.get("comment", "")
        if (execute and define_ret and isinstance(is_success, str)):
            if global_define:
                self.define(
                    define_name,
                    is_success,
                    quote=quote,
                    comment=comment
                )
            else:
                if quote:
                    succ = '"%s"' % is_success
                else:
                    succ = int(is_success)
                val = "%s=%s" % (define_name, succ)
                var = "DEFINES_%s" % uselib_store
                self.env.append_value(var, val)
        else:
            if global_define:
                self.define_cond(define_name, is_success, comment=comment)
            else:
                var = "DEFINES_%s" % uselib_store
                self.env.append_value(
                    var,
                    "%s=%s" % (define_name, int(is_success))
                )

        if uselib_store:
            self.env[self.have_define(uselib_store)] = 1
        elif execute and define_ret:
            self.env[define_name] = is_success
        else:
            self.env[define_name] = int(is_success)

    if header_name:
        if kw.get("auto_add_header_name"):
            self.env.append_value(
                INCKEYS,
                Utils.to_list(header_name)
            )

    if is_success and uselib_store:
        for k, v in get_uselib_config(uselib_store, kw):
            self.env.append_value(k, v)

    return is_success


@conf
def check(self, *k, **kw):
    """Performs a configuration test by calling
    :py:func:`waflib.Configure.run_build`.

    For the complete list of parameters, see
    :py:func:`waflib.Tools.c_config.validate_c`.

    To force a specific compiler, pass ``compiler='c'`` or
    ``compiler='cxx'`` to the list of arguments

    Besides build targets, complete builds can be given through a
    build function. All files will be written to a temporary
    directory::

        def build(bld):
            lib_node = bld.srcnode.make_node('libdir/liblc1.c')
            lib_node.parent.mkdir()
            lib_node.write(
                "#include <stdio.h>\\n"
                "int lib_func(void) { FILE *f = fopen("foo", "r");}\\n",
                "w"
            )
            bld(
                features=['c', 'cshlib'],
                source=[lib_node],
                linkflags=conf.env.EXTRA_LDFLAGS,
                target='liblc'
            )
            conf.check(build_fun=build, msg=msg)

    """
    validate_c(self, kw)
    self.start_msg(kw["msg"], **kw)
    ret = None
    try:
        ret = self.run_build(*k, **kw)
    except self.errors.ConfigurationError:
        self.end_msg(kw["errmsg"], "YELLOW", **kw)
        if Logs.verbose > 1:
            raise
        else:
            self.fatal("The configuration failed")
    else:
        kw["success"] = ret

    ret = self.post_check(**kw)

    if not ret:
        self.end_msg(kw["errmsg"], "YELLOW", **kw)
        self.fatal("The configuration failed %r" % ret)
    else:
        self.end_msg(kw["okmsg"], **kw)
    return ret


class test_exec(Task.Task):
    """A task that runs programs after they are built. See
    :py:func:`waflib.Tools.c_config.test_exec_fun`.
    """
    color = "PINK"

    def run(self):
        cmd = [self.inputs[0].abspath()] \
            + getattr(self.generator, "test_args", [])
        if getattr(self.generator, "rpath", None):
            if getattr(self.generator, "define_ret", False):
                self.generator.bld.retval = \
                    self.generator.bld.cmd_and_log(cmd)
            else:
                self.generator.bld.retval = \
                    self.generator.bld.exec_command(cmd)
        else:
            env = self.env.env or {}
            env.update(dict(os.environ))
            for var in ("LD_LIBRARY_PATH", "DYLD_LIBRARY_PATH", "PATH"):
                env[var] = (
                    self.inputs[0].parent.abspath() + os.path.pathsep + env.get(var, "")
                )
            if getattr(self.generator, "define_ret", False):
                self.generator.bld.retval = \
                    self.generator.bld.cmd_and_log(cmd, env=env)
            else:
                self.generator.bld.retval = \
                    self.generator.bld.exec_command(
                        cmd, env=env
                    )


@feature("test_exec")
@after_method("apply_link")
def test_exec_fun(self):
    """The feature **test_exec** is used to create a task that will
    to execute the binary created (link task output) during the
    build.

    The exit status will be set on the build context, so only one
    program may have the feature *test_exec*.  This is used by
    configuration tests::

        def configure(conf):
            conf.check(execute=True)
    """
    self.create_task("test_exec", self.link_task.outputs[0])


# Don't use this
@conf
def check_cxx(self, *k, **kw):
    """
    Runs a test with a task generator of the form::

        conf.check(features=['cxx', 'cxxprogram'], ...)
    """
    kw["compiler"] = "cxx"
    return self.check(*k, **kw)

# Or this
@conf
def check_cc(self, *k, **kw):
    """
    Runs a test with a task generator of the form::

        conf.check(features=['c', 'cprogram'], ...)
    """
    kw["compiler"] = "c"
    return self.check(*k, **kw)


@conf
def set_define_comment(self, key, comment):
    """
    Sets a comment that will appear in the configuration header

    :type key: string
    :type comment: string
    """
    coms = self.env.DEFINE_COMMENTS
    if not coms:
        coms = self.env.DEFINE_COMMENTS = {}
    coms[key] = comment or ""


@conf
def get_define_comment(self, key):
    """
    Returns the comment associated to a define

    :type key: string
    """
    coms = self.env.DEFINE_COMMENTS or {}
    return coms.get(key, "")


@conf
def define(self, key, val, quote=True, comment=""):
    """Stores a single define and its state into
    ``conf.env.DEFINES``. The value is cast to an integer (0/1).

    :param key: define name
    :type key: string
    :param val: value
    :type val: int or string
    :param quote: enclose strings in quotes (yes by default)
    :type quote: bool

    """
    assert key and isinstance(key, str)
    if val is True:
        val = 1
    elif val in (False, None):
        val = 0

    if isinstance(val, int) or isinstance(val, float):
        s = "%s=%s"
    else:
        s = quote and '%s="%s"' or "%s=%s"
    app = s % (key, str(val))

    ban = key + "="
    lst = self.env.DEFINES
    for x in lst:
        if x.startswith(ban):
            lst[lst.index(x)] = app
            break
    else:
        self.env.append_value("DEFINES", app)

    self.env.append_unique(DEFKEYS, key)
    self.set_define_comment(key, comment)


@conf
def undefine(self, key, comment=""):
    """
    Removes a global define from ``conf.env.DEFINES``

    :param key: define name
    :type key: string
    """
    assert isinstance(key, str)
    if not key:
        return
    ban = key + "="
    lst = [x for x in self.env["DEFINES"] if not x.startswith(ban)]
    self.env["DEFINES"] = lst
    self.env.append_unique(DEFKEYS, key)
    self.set_define_comment(key, comment)


@conf
def define_cond(self, key, val, comment=""):
    """
    Conditionally defines a name::

        def configure(conf):
            conf.define_cond('A', True)
            # equivalent to:
            # if val: conf.define('A', 1)
            # else: conf.undefine('A')

    :param key: define name
    :type key: string
    :param val: value
    :type val: int or string
    """
    assert isinstance(key, str)
    if not key:
        return
    if val:
        self.define(key, 1, comment=comment)
    else:
        self.undefine(key, comment=comment)


@conf
def is_defined(self, key):
    """Indicates whether a particular define is globally set in
    ``conf.env.DEFINES``.

    :param key: define name
    :type key: string
    :return: True if the define is set
    :rtype: bool

    """
    assert key and isinstance(key, str)

    ban = key + "="
    for x in self.env.DEFINES:
        if x.startswith(ban):
            return True
    return False


@conf
def get_define(self, key):
    """
    Returns the value of an existing define, or None if not found

    :param key: define name
    :type key: string
    :rtype: string
    """
    assert key and isinstance(key, str)

    ban = key + "="
    for x in self.env.DEFINES:
        if x.startswith(ban):
            return x[len(ban) :]
    return None


@conf
def have_define(self, key):
    """Returns a variable suitable for command-line or header use by
    removing invalid characters and prefixing it with ``HAVE_``

    :param key: define name
    :type key: string
    :return: the input key prefixed by *HAVE_* and substitute any
        invalid characters.
    :rtype: string
    """
    return (self.env.HAVE_PAT or "HAVE_%s") % Utils.quote_define_name(key)


@conf
def write_config_header(
    self,
    configfile="",
    guard="",
    top=False,
    defines=True,
    headers=False,
    remove=True,
    define_prefix="",
):
    """Writes a configuration header containing defines and includes::

        def configure(cnf):
            cnf.define('A', 1)
            cnf.write_config_header('config.h')

    This function only adds include guards (if necessary), consult
    :py:func:`waflib.Tools.c_config.get_config_header` for details on
    the body.

    :param configfile: path to the file to create (relative or absolute)
    :type configfile: string
    :param guard: include guard name to add, by default it is computed
        from the file name
    :type guard: string
    :param top: write the configuration header from the build directory
        (default is from the current path)
    :type top: bool
    :param defines: add the defines (yes by default)
    :type defines: bool
    :param headers: add #include in the file
    :type headers: bool
    :param remove: remove the defines after they are added (yes by
        default, works like in autoconf)
    :type remove: bool
    :type define_prefix: string
    :param define_prefix: prefix all the defines in the file with a
        particular prefix
    """
    assert configfile
    # if not configfile:
    #     configfile = WAF_CONFIG_H
    waf_guard = guard or "W_%s_WAF" % Utils.quote_define_name(configfile)

    node = top and self.bldnode or self.path.get_bld()
    node = node.make_node(configfile)
    node.parent.mkdir()

    lst = ["/* WARNING! All changes made to this file will be lost! */\n"]
    lst.append("#ifndef %s\n#define %s\n" % (waf_guard, waf_guard))
    lst.append(self.get_config_header(
        defines, headers, define_prefix=define_prefix
    ))
    lst.append("\n#endif /* %s */\n" % waf_guard)

    node.write("\n".join(lst))

    # config files must not be removed on "waf clean"
    self.env.append_unique(Build.CFG_FILES, [node.abspath()])

    if remove:
        for key in self.env[DEFKEYS]:
            self.undefine(key)
        self.env[DEFKEYS] = []


@conf
def get_config_header(self, defines=True, headers=False, define_prefix=""):
    """Creates the contents of a ``config.h`` file from the defines
    and includes set in conf.env.define_key / conf.env.include_key. No
    include guards are added.

    A prelude will be added from the variable env.WAF_CONFIG_H_PRELUDE
    if provided. This can be used to insert complex macros or include
    guards::

        def configure(conf):
            conf.env.WAF_CONFIG_H_PRELUDE = '#include <unistd.h>\\n'
            conf.write_config_header('config.h')

    :param defines: write the defines values
    :type defines: bool
    :param headers: write include entries for each element in
        self.env.INCKEYS
    :type headers: bool
    :type define_prefix: string
    :param define_prefix: prefix all the defines with a particular prefix
    :return: the contents of a ``config.h`` file
    :rtype: string

    """
    lst = []

    if self.env.WAF_CONFIG_H_PRELUDE:
        lst.append(self.env.WAF_CONFIG_H_PRELUDE)

    if headers:
        for x in self.env[INCKEYS]:
            lst.append("#include <%s>" % x)

    if defines:
        tbl = {}
        for k in self.env.DEFINES:
            a, _, b = k.partition("=")
            tbl[a] = b

        for k in self.env[DEFKEYS]:
            caption = self.get_define_comment(k)
            if caption:
                caption = " /* %s */" % caption
            try:
                txt = "#define %s%s %s%s" \
                    % (define_prefix, k, tbl[k], caption)
            except KeyError:
                txt = "/* #undef %s%s */%s" % (define_prefix, k, caption)
            lst.append(txt)
    return "\n".join(lst)


@conf
def cc_add_flags(conf):
    """
    Adds CFLAGS / CPPFLAGS from os.environ to conf.env
    """
    conf.add_os_flags("CPPFLAGS", dup=False)
    conf.add_os_flags("CFLAGS", dup=False)


@conf
def cxx_add_flags(conf):
    """
    Adds CXXFLAGS / CPPFLAGS from os.environ to conf.env
    """
    conf.add_os_flags("CPPFLAGS", dup=False)
    conf.add_os_flags("CXXFLAGS", dup=False)


@conf
def link_add_flags(conf):
    """
    Adds LINKFLAGS / LDFLAGS from os.environ to conf.env
    """
    conf.add_os_flags("LINKFLAGS", dup=False)
    conf.add_os_flags("LDFLAGS", dup=False)


@conf
def cc_load_tools(conf):
    """
    Loads the Waf c extensions
    """
    if not conf.env.DEST_OS:
        conf.env.DEST_OS = Utils.unversioned_sys_platform()
    conf.load_tool("c")


@conf
def cxx_load_tools(conf):
    """
    Loads the Waf c++ extensions
    """
    if not conf.env.DEST_OS:
        conf.env.DEST_OS = Utils.unversioned_sys_platform()
    conf.load_tool("cxx")

def read_compiler_macros(conf, cc):
    args = cc + conf.env["CFLAGS"] + conf.env["CXXFLAGS"] \
        + ["-dM", "-E", "-"]

    out, err = conf.cmd_and_log(args, output=0, input="\n".encode())

    ms = {}
    out = out.splitlines()
    for line in out:
        lst = shlex.split(line)
        if len(lst) > 2:
            key = lst[1]
            val = lst[2]
            ms[key] = val
    return ms

def ensure_compiler_is(conf, ms, compiler):
    if compiler == 'gcc':
        if "__INTEL_COMPILER" in ms:
            conf.fatal("The intel compiler pretends to be gcc")
        if not "__GNUC__" in ms and not "__clang__" in ms:
            conf.fatal("Could not determine the compiler type")

    if compiler == "icc" and not "__INTEL_COMPILER" in ms:
        conf.fatal("Not icc/icpc")

    if compiler == "clang" and not "__clang__" in ms:
        conf.fatal("Not clang/clang++")
    if compiler != "clang" and "__clang__" in ms:
        conf.fatal(
            "Could not find gcc/g++ (only Clang), "
            "if renamed try eg: CC=gcc48 CXX=g++48 waf configure"
        )

def get_dest_os(ms):
    # Some documentation is available at http://predef.sourceforge.net
    # The names given to DEST_OS must match what
    # Utils.unversioned_sys_platform() returns.
    for k, v in MACRO_TO_DEST_OS.items():
        if k in ms:
            return v
    if "__APPLE__" in ms and "__MACH__" in ms:
        return "darwin"
    # unix must be tested last as it's a generic fallback
    if "__unix__" in ms:
        return "generic"
    return "unknown"

def get_dest_binfmt(ms, dest_os):
    if "__ELF__" in ms:
        return "elf"
    elif "__WINNT__" in ms or "__CYGWIN__" in ms or "_WIN32" in ms:
        return "pe"
    elif "__APPLE__" in ms:
        return "mac-o"
    return Utils.destos_to_binfmt(dest_os)

def get_dest_cpu(ms):
    for k, v in MACRO_TO_DEST_CPU.items():
        if k in ms:
            return v
    return "unknown"

def get_compiler_version(ms, compiler):
    if compiler == "icc":
        ver = ms["__INTEL_COMPILER"]
        return ver[:-2], ver[-2], ver[-1]
    if "__clang__" in ms and "__clang_major__" in ms:
        return (ms["__clang_major__"], ms["__clang_minor__"],
                ms["__clang_patchlevel__"])
    # older clang versions and gcc
    return (ms["__GNUC__"], ms["__GNUC_MINOR__"],
            ms.get("__GNUC_PATCHLEVEL__", "0"))

@conf
def get_cc_version(self, cc, expected_cc):
    """Runs the preprocessor to determine the gcc/icc/clang version

    Sets the variables CC_VERSION, DEST_OS, DEST_BINFMT, DEST_CPU, and
    DEST_CPU_EXTS in *conf.env*. The parameter `expected_cc` should be
    one of "gcc", "clang", or "icc". conf.env.CFLAGS and/or
    conf.env.CXXFLAGS should be set before this function runs.

    :raise: :py:class:`waflib.Errors.ConfigurationError`
    """
    ms = read_compiler_macros(self, cc)
    ensure_compiler_is(self, ms, expected_cc)

    dest_os = get_dest_os(ms)
    dest_binfmt = get_dest_binfmt(ms, dest_os)
    dest_cpu = get_dest_cpu(ms)

    E = self.env
    if dest_binfmt == "pe":
        if not E["IMPLIBDIR"]:
            E["IMPLIBDIR"] = E["LIBDIR"]
        E["LIBDIR"] = E["BINDIR"]
    E["DEST_OS"] = dest_os
    E["DEST_BINFMT"] = dest_binfmt
    E["DEST_CPU"] = dest_cpu
    E["CC_VERSION"] = get_compiler_version(ms, expected_cc)
    E["DEST_CPU_EXTS"] = DEST_CPU_EXTS_MACROS & ms.keys()

@conf
def get_xlc_version(conf, cc):
    """
    Returns the Aix compiler version

    :raise: :py:class:`waflib.Errors.ConfigurationError`
    """
    cmd = cc + ["-qversion"]
    try:
        out, err = conf.cmd_and_log(cmd, output=0)
    except Errors.WafError:
        conf.fatal("Could not find xlc %r" % cmd)

    # the intention is to catch the 8.0 in "IBM XL C/C++ Enterprise
    # Edition V8.0 for AIX..."
    for v in (r"IBM XL C/C\+\+.* V(?P<major>\d*)\.(?P<minor>\d*)",):
        version_re = re.compile(v, re.I).search
        match = version_re(out or err)
        if match:
            k = match.groupdict()
            conf.env.CC_VERSION = (k["major"], k["minor"])
            break
    else:
        conf.fatal("Could not determine the XLC version.")


@conf
def get_suncc_version(conf, cc):
    """
    Returns the Sun compiler version

    :raise: :py:class:`waflib.Errors.ConfigurationError`
    """
    cmd = cc + ["-V"]
    try:
        out, err = conf.cmd_and_log(cmd, output=0)
    except Errors.WafError as e:
        # Older versions of the compiler exit with non-zero status
        # when reporting their version
        if not (
            hasattr(e, "returncode")
                and hasattr(e, "stdout")
                and hasattr(e, "stderr")
        ):
            conf.fatal("Could not find suncc %r" % cmd)
        out = e.stdout
        err = e.stderr

    version = out or err
    version = version.splitlines()[0]

    # cc: Sun C 5.10 SunOS_i386 2009/06/03
    # cc: Studio 12.5 Sun C++ 5.14 SunOS_sparc Beta 2015/11/17
    # cc: WorkShop Compilers 5.0 98/12/15 C 5.0
    version_re = re.compile(
        r"cc: (studio.*?|\s+)?(sun\s+(c\+\+|c)|(WorkShop\s+Compilers))?\s+(?P<major>\d*)\.(?P<minor>\d*)",
        re.I,
    ).search
    match = version_re(version)
    if match:
        k = match.groupdict()
        conf.env.CC_VERSION = (k["major"], k["minor"])
    else:
        conf.fatal("Could not determine the suncc version.")


# ============ the --as-needed flag should added during the configuration, not at runtime =========


@conf
def add_as_needed(self):
    """Adds ``--as-needed`` to the *LINKFLAGS*

    On some platforms, it is a default flag.  In some cases (e.g., in
    NS-3) it is necessary to explicitly disable this feature with
    `-Wl,--no-as-needed` flag.
    """
    if self.env.DEST_BINFMT == "elf" and "gcc" in (self.env.CXX_NAME, self.env.CC_NAME):
        self.env.append_unique("LINKFLAGS", "-Wl,--as-needed")


# ============ parallel configuration


class cfgtask(Task.Task):
    """A task that executes build configuration tests (calls conf.check)

    Make sure to use locks if concurrent access to the same conf.env
    data is necessary.

    """

    def __init__(self, *k, **kw):
        Task.Task.__init__(self, *k, **kw)
        self.run_after = set()

    def display(self):
        return ""

    def runnable_status(self):
        for x in self.run_after:
            if not x.hasrun:
                return Task.ASK_LATER
        return Task.RUN_ME

    def uid(self):
        return Utils.SIG_NIL

    def signature(self):
        return Utils.SIG_NIL

    def run(self):
        conf = self.conf
        bld = Build.BuildContext(
            top_dir=conf.srcnode.abspath(), out_dir=conf.bldnode.abspath()
        )
        bld.env = conf.env
        bld.init_dirs()
        bld.in_msg = 1  # suppress top-level start_msg
        bld.logger = self.logger
        bld.multicheck_task = self
        args = self.args
        try:
            if "func" in args:
                bld.test(
                    build_fun=args["func"],
                    msg=args.get("msg", ""),
                    okmsg=args.get("okmsg", ""),
                    errmsg=args.get("errmsg", ""),
                )
            else:
                args["multicheck_mandatory"] = args.get("mandatory", True)
                args["mandatory"] = True
                try:
                    bld.check(**args)
                finally:
                    args["mandatory"] = args["multicheck_mandatory"]
        except Exception:
            return 1

    def process(self):
        Task.Task.process(self)
        if "msg" in self.args:
            with self.generator.bld.multicheck_lock:
                self.conf.start_msg(self.args["msg"])
                if self.hasrun == Task.NOT_RUN:
                    self.conf.end_msg("test cancelled", "YELLOW")
                elif self.hasrun != Task.SUCCESS:
                    self.conf.end_msg(
                        self.args.get("errmsg", "no"), "YELLOW")
                else:
                    self.conf.end_msg(
                        self.args.get("okmsg", "yes"), "GREEN")


@conf
def multicheck(self, *k, **kw):
    """Runs configuration tests in parallel; results are printed
    sequentially at the end of the build but each test must provide
    its own msg value to display a line::

        def test_build(ctx):
            ctx.in_msg = True # suppress console outputs
            ctx.check_large_file(mandatory=False)

            conf.multicheck(
                {
                    'header_name':'stdio.h',
                    'msg':'... stdio',
                    'uselib_store':'STDIO',
                    'global_define':False
                },
                {
                    'header_name':'xyztabcd.h',
                    'msg':'... optional xyztabcd.h',
                    'mandatory': False
                },
                {
                    'header_name':'stdlib.h',
                    'msg':'... stdlib',
                    'okmsg': 'aye',
                    'errmsg': 'nope'
                },
                {
                    'func': test_build,
                    'msg':'... testing an arbitrary build function',
                    'okmsg':'ok'
                },
                msg       = 'Checking for headers in parallel',
                # mandatory tests raise an error at the end
                mandatory = True,
                run_all_tests = True, # try running all tests
            )

    The configuration tests may modify the values in conf.env in any
    order, and the define values can affect configuration tests being
    executed. It is hence recommended to provide `uselib_store` values
    with `global_define=False` to prevent such issues.
    """
    self.start_msg(
        kw.get("msg", "Executing %d configuration tests" % len(k)),
        **kw
    )

    # Force a copy so that threads append to the same list at least no
    # order is guaranteed, but the values should not disappear at
    # least
    for var in ("DEFINES", DEFKEYS):
        self.env.append_value(var, [])
    self.env.DEFINE_COMMENTS = self.env.DEFINE_COMMENTS or {}

    # define a task object that will execute our tests
    class par(object):
        def __init__(self):
            self.keep = False
            self.task_sigs = {}
            self.progress_bar = 0

        def total(self):
            return len(tasks)

        def to_log(self, *k, **kw):
            return

    bld = par()
    bld.keep = kw.get("run_all_tests", True)
    bld.imp_sigs = {}
    tasks = []

    id_to_task = {}
    for counter, dct in enumerate(k):
        x = Task.classes["cfgtask"](bld=bld, env=None)
        tasks.append(x)
        x.args = dct
        x.args["multicheck_counter"] = counter
        x.bld = bld
        x.conf = self
        x.args = dct

        # bind a logger that will keep the info in memory
        x.logger = Logs.make_mem_logger(str(id(x)), self.logger)

        if "id" in dct:
            id_to_task[dct["id"]] = x

    # second pass to set dependencies with after_test/before_test
    for x in tasks:
        for key in Utils.to_list(x.args.get("before_tests", [])):
            tsk = id_to_task[key]
            if not tsk:
                raise ValueError("No test named %r" % key)
            tsk.run_after.add(x)
        for key in Utils.to_list(x.args.get("after_tests", [])):
            tsk = id_to_task[key]
            if not tsk:
                raise ValueError("No test named %r" % key)
            x.run_after.add(tsk)

    def it():
        yield tasks
        while 1:
            yield []

    bld.producer = p = Runner.Parallel(bld, Options.options.jobs)
    bld.multicheck_lock = Utils.threading.Lock()
    p.biter = it()

    self.end_msg("started")
    p.start()

    # flush the logs in order into the config.log
    for x in tasks:
        x.logger.memhandler.flush()

    self.start_msg("-> processing test results")
    if p.error:
        for x in p.error:
            if getattr(x, "err_msg", None):
                self.to_log(x.err_msg)
                self.end_msg("fail", color="RED")
                raise Errors.WafError(
                    "There is an error in the library, "
                    "read config.log for more information"
                )

    failure_count = 0
    for x in tasks:
        if x.hasrun not in (Task.SUCCESS, Task.NOT_RUN):
            failure_count += 1

    if failure_count:
        self.end_msg(
            kw.get("errmsg", "%s test failed" % failure_count),
            color="YELLOW",
            **kw
        )
    else:
        self.end_msg("all ok", **kw)

    for x in tasks:
        if x.hasrun != Task.SUCCESS:
            if x.args.get("mandatory", True):
                self.fatal(
                    kw.get("fatalmsg")
                    or "One of the tests has failed, read config.log for more information"
                )


@conf
def check_gcc_o_space(self, mode="c"):
    if int(self.env.CC_VERSION[0]) > 4:
        # this is for old compilers
        return
    self.env.stash()
    if mode == "c":
        self.env.CCLNK_TGT_F = ["-o", ""]
    elif mode == "cxx":
        self.env.CXXLNK_TGT_F = ["-o", ""]
    features = "%s %sshlib" % (mode, mode)
    try:
        self.check(
            msg="Checking if the -o link must be split from arguments",
            fragment=SNIP_EMPTY_PROGRAM,
            features=features,
        )
    except self.errors.ConfigurationError:
        self.env.revert()
    else:
        self.env.commit()
