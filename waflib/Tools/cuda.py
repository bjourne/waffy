# Copyright (C) 2010 Thomas Nagy (ita)
# Copyright (C) 2025 Björn A. Lindqvist (bjourne)
from pathlib import Path
from waflib import Task
from waflib.TaskGen import set_task_mappings
from waflib.Tools import ccroot, c_preproc
from waflib.Tools.ccroot import create_compiled_task
from waflib.Configure import conf

class cuda(Task.Task):
    run_str = " ".join([
        "${NVCC}",
        "${CUDAFLAGS}",
        "${NVCCFLAGS_ST:CXXFLAGS}",
        "${FRAMEWORKPATH_ST:FRAMEWORKPATH}",
        "${CPPPATH_ST:INCPATHS}",
        "${DEFINES_ST:DEFINES}",
        "${CXX_SRC_F}${SRC}",
        "${CXX_TGT_F}",
        "${TGT}"
    ])
    color = "GREEN"
    ext_in = [".h"]
    vars = ["CCDEPS"]
    scan = c_preproc.scan
    shell = False


def handle_cu_and_cuda(self, node):
    return create_compiled_task(self, "cuda", node)
set_task_mappings([".cu", ".cuda"], handle_cu_and_cuda)


def handle_cpp(self, node):
    # override processing for one particular type of file
    if hasattr(self, "cuda"):
        return create_compiled_task(self, "cuda", node)
    return create_compiled_task(self, "cxx", node)
set_task_mappings([".cpp"], handle_cpp)


def configure(conf):
    conf.find_program("nvcc", var="NVCC")
    conf.find_cuda_libs()
    conf.env.NVCCFLAGS_ST = "--compiler-options=%s"

@conf
def find_cuda_libs(self):
    """Finds CUDA headers and libraries. Defines the uselib variables
    CUDA and CUDART. Use

        bld(source = ["main.c"],
            target="app",
            cuda=True,
            use=["CUDA", "CUDART"])
    """
    prefix = Path(self.env["NVCC"][0]).parent.parent

    include = prefix / "include"
    includes = [str(include)] if include.exists() else []

    libpath = [prefix / x for x in ["lib64", "lib64/stubs", "lib", "lib/stubs"]]
    libpath = [str(p) for p in libpath if p.exists()]

    # this should not raise any error
    cuda_libs = [
        ("cuda.h", "cuda"),
        ([], "cudart"),
        ("cusparse.h", "cusparse"),
        ("cublas_v2.h", "cublas")
    ]
    for header_name, lib in cuda_libs:
        self.check(
            header_name=header_name,
            lib=lib,
            libpath=libpath,
            includes=includes
        )
