from waflib.Utils import toposort

def test_toposort():
    G = {
        'process_rule': {'process_source'},
        'process_subst': {'process_source', 'process_rule'},
        'process_install_task': {'process_source', 'process_rule'},
        'apply_link': {
            'grep_for_endianness_fun', 'apply_implib',
            'test_exec_fun', 'set_full_paths_hpux',
            'create_task_macapp', 'process_use', 'apply_vnum',
            'create_task_macplist'
        },
        'apply_bundle': {'propagate_uselib_vars', 'apply_link'},
        'link_lib_test_fun': {'process_source'},
        'propagate_uselib_vars': {'apply_incpaths', 'apply_vnum'},
        'process_source': {'apply_incpaths', 'process_use', 'apply_link'},
        'apply_skip_stlib_link_deps': {'process_use'},
        'process_use': {
            'apply_incpaths', 'propagate_uselib_vars',
            'set_full_paths_hpux'
        },
        'process_objs': {'process_source'}
    }
    meths = {
        'apply_bundle', 'apply_incpaths', 'apply_vnum', 'apply_link',
        'apply_implib',
        'check_err_features', 'check_err_order',
        'process_rule', 'process_source', 'process_use',
        'propagate_uselib_vars',
        'set_macosx_deployment_target'
    }
    _, lst = toposort(meths, G)
    assert lst.index("apply_bundle") < lst.index("apply_link")
    assert lst.index("propagate_uselib_vars") < lst.index("apply_incpaths")

    G = {'a' : {'b'},
         'b' : {'a'}}
    ok, lst = toposort(['a', 'b'], G)
    assert not ok
