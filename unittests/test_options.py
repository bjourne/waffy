from waflib import Options

def test_options_context():
    oc = Options.OptionsContext()
    oc.parse_args(_args = [])
    assert Options.options.prefix == '/usr/local'
