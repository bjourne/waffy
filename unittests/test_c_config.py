# Copyright (C) 2024-2025 Björn A. Lindqvist (bjourne)
from waflib.Tools.c_config import (
    get_check_message,
    get_have_define,
    get_features,
    get_uselib_store,
    get_uselib_config,
    parse_c_flags,
    set_uselib_c_flags
)

def test_parse_c_flags():
    d = parse_c_flags("-O3 -ldl -lm", False, False, False)
    assert d["CXXFLAGS"] == ["-O3"]
    assert d["CFLAGS"] == ["-O3"]
    assert d["LINKFLAGS"] == []
    assert d["LIB"] == ["dl", "m"]
    assert d["LIBPATH"] == []

def test_parse_c_flags_lib_paths():
    d = parse_c_flags("-L/usr/lib -L/usr/lib -ldl  -lm", False, False, False)
    assert d["LIBPATH"] == ["/usr/lib"]

def test_parse_c_flags_force_static():
    d = parse_c_flags("-O3 -ldl -lm", False, True, False)
    assert d["CXXFLAGS"] == ["-O3"]
    assert d["CFLAGS"] == ["-O3"]
    assert d["STLIB"] == ["dl", "m"]
    assert d["STLIBPATH"] == []

def test_parse_c_flags_python_config():
    line = " ".join([
        "-I/usr/include/python3.11",
        "-I/usr/include/python3.11",
        "-march=x86-64",
        "-mtune=generic",
        "-O3",
        "-pipe",
        "-fno-plt",
        "-fexceptions",
        "-Wp,-D_FORTIFY_SOURCE=2",
        "-Wformat",
        "-Werror=format-security",
        "-fstack-clash-protection",
        "-fcf-protection",
        "-g",
        "-ffile-prefix-map=/build/python/src=/usr/src/debug/python",
        "-flto=auto",
        "-ffat-lto-objects",
        "-DNDEBUG",
        "-g",
        "-fwrapv",
        "-O3",
        "-Wall",
        "-ldl",
        "-lm"
    ])
    d = parse_c_flags(line, False, False, False)
    assert d["INCLUDES"] == ["/usr/include/python3.11"]
    assert d["DEFINES"] == ["NDEBUG"]

def test_set_uselib_c_flags():
    old = {"INCLUDES_foo" : ["path"]}
    new = {"INCLUDES" : ["path2"]}

    # Redefinitions are not ok.
    try:
        set_uselib_c_flags(old, "foo", new)
        assert False
    except ValueError:
        pass

    # This should retain the old value.
    old = {"INCLUDES_foo" : ["path"]}
    new = {"INCLUDES" : []}
    set_uselib_c_flags(old, "foo", new)
    assert old["INCLUDES_foo"] == ["path"]

def test_get_check_message():
    setups = [
        (
            {'lib' : 'blah'},
            "Checking for library blah"
        ),
        (
            {'lib' : 'gomp', 'header_name' : 'eh.h'},
            "Checking for header eh.h and library gomp"
        ),
        (
            {
                "lib" : "sycl",
                "header_name" : "CL/sycl.hpp",
                "cxxflags" : [
                    '-Wno-attributes', '-Wno-deprecated-declarations'
                ]
            },
            "Checking for header CL/sycl.hpp, "
            "library sycl, and "
            "C++ compiler flag(s) -Wno-attributes and "
            "-Wno-deprecated-declarations"
        ),
        (
            {"fragment" : "bla bla bla"},
            "Checking for code snippet"
        )
    ]
    for data, text in setups:
        assert get_check_message(data) == text

def test_get_uselib_store():
    assert get_uselib_store({"uselib_store" : "foo"}) == "foo"
    assert get_uselib_store({"lib" : "bar"}) == "BAR"
    assert get_uselib_store({"lib" : "bar", "header" : "hdr"}) == "BAR"
    assert get_uselib_store({"header_name" : "CL/cl.h"}) == "CL/CL.H"

def test_get_features():
    setups = [
        ({"features" : ["foo"]}, ["foo"]),
        ({"features" : ["foo"], "execute" : True}, ["foo", "test_exec"]),
        ({"features" : [], "type" : "foo", "compilers" : ["blah"]},
         ["blah", "foo"])
    ]
    for setup, lst in setups:
        assert get_features(setup) == lst

def test_get_uselib_config():
    d = {"features" : ["c"], "lib" : "m"}
    assert dict(get_uselib_config("US", d)) == {}
    d = {"features" : ["cprogram"], "lib" : "m"}
    assert dict(get_uselib_config("US", d)) == {
        "LIB_US" : "m"
    }

def test_get_have_define():
    d = {"header_name" : "math.h"}
    assert get_have_define(d) == "HAVE_MATH_H"
    d = {"uselib_store" : "GOMP"}
    assert get_have_define(d) == "HAVE_GOMP"

    # uselib_store takes precedence over header_name
    d = {"uselib_store" : "CUDA", "header_name" : "cuda.h"}
    assert get_have_define(d) == "HAVE_CUDA"

    # as does lib
    d = {"lib" : "CUDA", "header_name" : "cuda.h"}
    assert get_have_define(d) == "HAVE_CUDA"
