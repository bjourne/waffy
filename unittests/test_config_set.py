from waflib.ConfigSet import ConfigSet

def test_config_set():
    cs = ConfigSet()
    assert not "foo" in cs
    cs["foo"] = ["abc"]
    assert "foo" in cs
