#include <stdio.h>

#include "a.h"
#include "config.h"
int main() {
    size_t sizes[] = {
        INT_SIZE,
        LONG_INT_SIZE,
        LONG_LONG_INT_SIZE
    };
    char *names[] = {
        "int",
        "long int",
        "long long int"
    };
    printf("Variant: %s\n", VARIANT_NAME);
    printf("Sizes:\n");
    for (int i = 0; i < 3; i++) {
        printf("    %-15s: %d\n", names[i], sizes[i]);
    }
    return 0;
}
