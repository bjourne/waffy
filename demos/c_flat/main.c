#include <math.h>
#include <stdio.h>

void fun();

int
main() {
    printf("Support: PNG = %d, GOMP = %d\n", HAVE_PNG, HAVE_GOMP);
    printf("tanh(0.8) = %8.3f\n", tanh(0.8));
}
